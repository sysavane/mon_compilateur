			# This code was produced by the CERI Compiler
.data
FormatString1:	.string "%llu"	# used by printf to display 64-bit unsigned integers
FormatString2:	.string "%lf"	# used by printf to display 64-bit floating point numbers
FormatString3:	.string "%c"	# used by printf to display a 8-bit single character
TrueString:	.string "TRUE"	# used by printf to display the boolean value TRUE
FalseString:	.string "FALSE"	# used by printf to display the boolean value FALSE
a:	.quad 0
b:	.quad 0
	.align 8
	.text		# The following lines contain the program
	.globl main	# The main function must be visible from outside
main:			# The main function body :
	movq %rsp, %rbp	# Save the position of the stack's top
CONST0:
	push $12
	pop %rax
	push $1
	pop a
CASE1:
	push a
	pop %rax
	push $1
	pop %rbx
	cmpq %rax, %rbx
	je DO3
	jmp Next2
DO3:
	push $3
	push $5
	pop %rbx
	pop %rax
	mulq	%rbx
	push %rax	# MUL
	pop b
	jmp END1
Next2:
	push $2
	pop %rbx
	cmpq %rax, %rbx
	je DO5
	jmp Next4
DO5:
	push $2
	push $3
	pop %rbx
	pop %rax
	addq	%rbx, %rax	# ADD
	push %rax
	pop b
Next4:
	push $3
	pop %rbx
	cmpq %rax, %rbx
	je DO7
	jmp Next6
DO7:
	push $5
	push $3
	pop %rbx
	pop %rax
	subq	%rbx, %rax	# ADD
	push %rax
	pop b
Next6:
END1:
	push b
	pop %rsi	# The value to be displayed
	movq $FormatString1, %rdi	# "%llu\n"
	movl	$0, %eax
	call	printf@PLT
	movq %rbp, %rsp		# Restore the position of the stack's top
	ret			# Return from main function
